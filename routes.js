const fs = require("fs");

const requestHandler = (req, res) => {
  const url = req.url;
  const method = req.method;
  if (url === "/") {
    res.write("<html>");
    res.write("<head><title>Node Js App</title></head>");
    res.write("<body><h1>Hello NodeJs App</h1></body>");
    res.write("</html>");
    return res.end();
  }

  if (url === "/post-message" && method === "POST") {
    const body = [];
    req.on("data", (chunk) => {
      body.push(chunk);
    });
    return req.on("end", () => {
      const parsedBody = Buffer.concat(body).toString();
      console.log(parsedBody);
      const email = parsedBody.split("=")[0];
      const message = parsedBody.split("=")[1];
      fs.writeFile("message.txt", JSON.stringify({ email, message }), (err) => {
        res.statusCode = 302;

        return res.end("Your Request Submited");
      });
    });
  }
};

exports.handler = requestHandler;
